package edu.bbte.patterns.visitor;

public interface IPurchasable {
    Double getPrice();
    Double accept(IPurchasableVisitor purchasableVisitor);
}
