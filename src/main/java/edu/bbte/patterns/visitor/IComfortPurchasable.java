package edu.bbte.patterns.visitor;

import edu.bbte.patterns.visitor.IPurchasable;

public interface IComfortPurchasable extends IPurchasable {
}