package edu.bbte.patterns.visitor;
public interface IPurchasableVisitor {
    Double visit(final IComfortPurchasable comfortPurchasable);
    Double visit(final IElectronicPurchasable electronicPurchasable);
    Double visit(final IFoodPurchasable foodPurchasable);
}
