package edu.bbte.patterns.visitor;

public interface IElectronicPurchasable extends IPurchasable {
    Double getPrice();
}