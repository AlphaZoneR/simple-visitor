package edu.bbte.patterns.visitor.visitor;

import edu.bbte.patterns.visitor.IComfortPurchasable;
import edu.bbte.patterns.visitor.IElectronicPurchasable;
import edu.bbte.patterns.visitor.IFoodPurchasable;
import edu.bbte.patterns.visitor.IPurchasableVisitor;

public class PersonPurchasableVisitor implements IPurchasableVisitor {

    @Override
    public Double visit(final IComfortPurchasable comfortPurchasable) {
        return comfortPurchasable.getPrice() * 1.19;
    }

    @Override
    public Double visit(final IElectronicPurchasable electronicPurchasable) {
        return electronicPurchasable.getPrice() * 1.19;
    }

    @Override
    public Double visit(final IFoodPurchasable foodPurchasable) {
        return foodPurchasable.getPrice() * 1.09;
    }
}
