package edu.bbte.patterns.visitor.purchasable;

import edu.bbte.patterns.visitor.IFoodPurchasable;
import edu.bbte.patterns.visitor.IPurchasableVisitor;

public class Banana implements IFoodPurchasable {
    private final Double price;

    public Banana(final Double price) {
        this.price = price;
    }

    @Override
    public Double getPrice() {
        return this.price;
    }

    @Override
    public Double accept(final IPurchasableVisitor purchasableVisitor) {
        return purchasableVisitor.visit(this);
    }
}
