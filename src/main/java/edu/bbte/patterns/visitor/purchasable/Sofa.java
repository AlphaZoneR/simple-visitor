package edu.bbte.patterns.visitor.purchasable;

import edu.bbte.patterns.visitor.IComfortPurchasable;
import edu.bbte.patterns.visitor.IPurchasableVisitor;

public class Sofa implements IComfortPurchasable {
    private final Double price;

    public Sofa(final Double price) {
        this.price = price;
    }

    @Override
    public Double getPrice() {
        return this.price;
    }

    @Override
    public Double accept(final IPurchasableVisitor purchasableVisitor) {
        return purchasableVisitor.visit(this);
    }
}
