package edu.bbte.patterns.visitor.purchasable;

import edu.bbte.patterns.visitor.IElectronicPurchasable;
import edu.bbte.patterns.visitor.IPurchasableVisitor;

public class Laptop implements IElectronicPurchasable {
    private final Double price;

    public Laptop(final Double price) {
        this.price = price;
    }

    @Override
    public Double getPrice() {
        return this.price;
    }

    @Override
    public Double accept(final IPurchasableVisitor purchasableVisitor) {
        return purchasableVisitor.visit(this);
    }
}
