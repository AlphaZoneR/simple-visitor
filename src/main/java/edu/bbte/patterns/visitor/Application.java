package edu.bbte.patterns.visitor;

import com.google.common.collect.ImmutableList;
import edu.bbte.patterns.visitor.purchasable.Banana;
import edu.bbte.patterns.visitor.purchasable.Laptop;
import edu.bbte.patterns.visitor.purchasable.Sofa;
import edu.bbte.patterns.visitor.visitor.CompanyPurchasableVisitor;
import edu.bbte.patterns.visitor.visitor.PersonPurchasableVisitor;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class Application {
    private static final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws IOException {
        List<IPurchasable> shoppingCart = ImmutableList.of(
                new Banana(0.9),
                new Sofa(520.0),
                new Laptop(3000.0));

        System.out.print("Is this a company? (Yes/No) ...");
        final String input = reader.readLine().toLowerCase();

        final IPurchasableVisitor purchasableVisitor;

        if (input.contains("yes") || input.equals("y")) {
            purchasableVisitor = new CompanyPurchasableVisitor();
        } else {
            purchasableVisitor = new PersonPurchasableVisitor();
        }

        final double total = shoppingCart.stream().map(i -> i.accept(purchasableVisitor))
                                         .mapToDouble(Double::doubleValue).sum();
        System.out.println("Total price is: " + total);
    }
}
